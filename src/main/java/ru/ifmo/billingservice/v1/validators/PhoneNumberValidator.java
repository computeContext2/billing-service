package ru.ifmo.billingservice.v1.validators;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;

public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {

    private final PhoneNumberUtil phoneNumberUtil;

    @Autowired
    public PhoneNumberValidator(PhoneNumberUtil phoneNumberUtil) {
        this.phoneNumberUtil = phoneNumberUtil;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        Phonenumber.PhoneNumber phoneNumber;
        try {
            phoneNumber = phoneNumberUtil.parse(value, "ZZ");
        } catch (NumberParseException e) {
            return false;
        }
        if (!phoneNumberUtil.isValidNumber(phoneNumber)) {
            return false;
        }
        return value.equals(phoneNumberUtil.format(phoneNumber, PhoneNumberFormat.E164));
    }
}
