package ru.ifmo.billingservice.v1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import ru.ifmo.billingservice.v1.models.Asset;
import ru.ifmo.billingservice.v1.models.Client;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public interface AssetRepository extends JpaRepository<Asset, UUID> {

    Stream<Asset> findByClientAndBalanceGreaterThanAndValidFromLessThanEqualAndValidToGreaterThan(
            @NonNull Client client,
            @NonNull Double balanceGreaterThan,
            @NonNull OffsetDateTime validFromLessThanEqual,
            @NonNull OffsetDateTime validToGreaterThan);

    Stream<Asset> findByClientAndAssetTypeAndBalanceGreaterThanAndValidFromLessThanEqualAndValidToGreaterThan(
            @NonNull Client client,
            @NonNull Asset.AssetType assetType,
            @NonNull Double balanceGreaterThan,
            @NonNull OffsetDateTime validFromLessThanEqual,
            @NonNull OffsetDateTime validToGreaterThan);

    @Transactional
    default List<Asset> findAvailable(@NonNull Client client) {
        OffsetDateTime now = OffsetDateTime.now();
        return findByClientAndBalanceGreaterThanAndValidFromLessThanEqualAndValidToGreaterThan(
                client, 0d, now, now).collect(Collectors.toList());
    }

    default Stream<Asset> listAvailable(@NonNull Client client, @NonNull Asset.AssetType assetType) {
        OffsetDateTime now = OffsetDateTime.now();
        return findByClientAndAssetTypeAndBalanceGreaterThanAndValidFromLessThanEqualAndValidToGreaterThan(
                client, assetType, 0d, now, now);
    }
}
