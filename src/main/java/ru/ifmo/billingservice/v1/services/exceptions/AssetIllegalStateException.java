package ru.ifmo.billingservice.v1.services.exceptions;

import ru.ifmo.billingservice.v1.services.exceptions.commons.AssetException;

public class AssetIllegalStateException extends AssetException {

    public AssetIllegalStateException(String message) {
        super(message);
    }
}
