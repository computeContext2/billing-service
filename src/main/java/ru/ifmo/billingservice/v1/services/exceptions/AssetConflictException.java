package ru.ifmo.billingservice.v1.services.exceptions;

import ru.ifmo.billingservice.v1.services.exceptions.commons.AssetException;

public class AssetConflictException extends AssetException {

    public AssetConflictException(Throwable cause) {
        super("Concurrent operation failed.", cause);
    }

    public AssetConflictException(String message, Throwable cause) {
        super(message, cause);
    }
}
