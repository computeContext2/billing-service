package ru.ifmo.billingservice.v1.services;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import ru.ifmo.billingservice.v1.models.Client;
import ru.ifmo.billingservice.v1.repositories.ClientRepository;
import ru.ifmo.billingservice.v1.services.exceptions.ClientConflictException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientDataException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientNotFoundException;

import java.util.List;
import java.util.function.UnaryOperator;

@Service
public class ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    private Client find(String phoneNumber, UnaryOperator<Client> operator) {
        return clientRepository.findByPhoneNumber(phoneNumber)
                .map(operator)
                .orElseThrow(ClientNotFoundException::new);
    }

    private Client save(Client client) {
        try {
            return clientRepository.save(client);
        } catch (DataIntegrityViolationException e) {
            throw new ClientConflictException("Constraint violation occurred.", e);
        } catch (OptimisticLockingFailureException e) {
            throw new ClientConflictException(e);
        }
    }

    public Client register(@NonNull Client client) {
        if (client.getPhoneNumber() == null) {
            throw new ClientDataException("Client phone number must not be blank.");
        }
        if (client.getIsLocked() == null) {
            client.setIsLocked(false);
        }
        return save(client);
    }

    public List<Client> getAll() {
        return clientRepository.findAll();
    }

    public Client get(@NonNull String phoneNumber) {
        return find(phoneNumber, UnaryOperator.identity());
    }

    public void update(@NonNull String phoneNumber, @NonNull Client client) {
        if (client.getPhoneNumber() != null && !client.getPhoneNumber().equals(phoneNumber)) {
            throw new ClientDataException("Client phone number must match or not be specified.");
        }
        save(find(phoneNumber, found -> {
            if (client.getIsLocked() != null) {
                found.setIsLocked(client.getIsLocked());
            }
            return found;
        }));
    }
}
