package ru.ifmo.billingservice.v1.services.exceptions;

import ru.ifmo.billingservice.v1.services.exceptions.commons.ClientException;

public class ClientNotFoundException extends ClientException {

    public ClientNotFoundException() {
        super("Client not found.");
    }
}
