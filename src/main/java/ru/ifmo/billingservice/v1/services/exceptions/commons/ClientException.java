package ru.ifmo.billingservice.v1.services.exceptions.commons;

public abstract class ClientException extends RuntimeException {

    protected ClientException(String message) {
        super(message);
    }

    protected ClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
