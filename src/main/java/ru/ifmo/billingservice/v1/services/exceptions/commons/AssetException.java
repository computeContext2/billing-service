package ru.ifmo.billingservice.v1.services.exceptions.commons;

public abstract class AssetException extends RuntimeException {

    protected AssetException(String message) {
        super(message);
    }

    protected AssetException(String message, Throwable cause) {
        super(message, cause);
    }
}
