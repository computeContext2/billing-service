package ru.ifmo.billingservice.v1.services.exceptions;

import ru.ifmo.billingservice.v1.services.exceptions.commons.ClientException;

public class ClientDataException extends ClientException {

    public ClientDataException(String message) {
        super(message);
    }
}
