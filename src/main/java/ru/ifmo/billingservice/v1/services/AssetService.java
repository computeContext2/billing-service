package ru.ifmo.billingservice.v1.services;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import ru.ifmo.billingservice.v1.models.Asset;
import ru.ifmo.billingservice.v1.models.AssetRepay;
import ru.ifmo.billingservice.v1.models.Client;
import ru.ifmo.billingservice.v1.repositories.AssetRepository;
import ru.ifmo.billingservice.v1.services.exceptions.AssetConflictException;
import ru.ifmo.billingservice.v1.services.exceptions.AssetIllegalStateException;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AssetService {

    private final ClientService clientService;
    private final AssetRepository assetRepository;

    @Autowired
    public AssetService(ClientService clientService, AssetRepository assetRepository) {
        this.clientService = clientService;
        this.assetRepository = assetRepository;
    }

    private Client findClient(String phoneNumber) {
        Client client = clientService.get(phoneNumber);
        if (client.getIsLocked()) {
            throw new AssetIllegalStateException("Client is locked out.");
        }
        return client;
    }

    private Asset save(Asset asset) {
        try {
            return assetRepository.save(asset);
        } catch (DataIntegrityViolationException e) {
            throw new AssetConflictException("Constraint violation occurred.", e);
        } catch (OptimisticLockingFailureException e) {
            throw new AssetConflictException(e);
        }
    }

    public Asset add(@NonNull String phoneNumber, @NonNull Asset asset) {
        asset.setClient(findClient(phoneNumber));
        asset.setBalance(asset.getInitBalance());
        return save(asset);
    }

    public List<Asset> getAvailable(@NonNull String phoneNumber) {
        return assetRepository.findAvailable(findClient(phoneNumber));
    }

    @Transactional
    public List<Asset> repay(@NonNull String phoneNumber, @NonNull Asset.AssetType assetType, @NonNull AssetRepay assetRepay) {
        List<Asset> assets = assetRepository.listAvailable(findClient(phoneNumber), assetType)
                .sorted(Comparator.comparing(Asset::getValidFrom).reversed())
                .dropWhile(asset -> {
                    if (assetRepay.getValue() == 0) {
                        return false;
                    }
                    assetRepay.setAsset(asset);
                    asset.getAssetRepays().add(assetRepay);
                    double newBalance = asset.getBalance() - assetRepay.getValue();
                    if (newBalance > 0) {
                        asset.setBalance(newBalance);
                        save(asset);
                        return false;
                    }
                    assetRepay.setValue(asset.getBalance());
                    asset.setBalance(0d);
                    save(asset);
                    assetRepay.setValue(-newBalance);
                    return true;
                })
                .collect(Collectors.toList());
        if (assets.isEmpty() && assetRepay.getValue() > 0) {
            throw new AssetIllegalStateException("No assets available.");
        }
        return assets;
    }
}
