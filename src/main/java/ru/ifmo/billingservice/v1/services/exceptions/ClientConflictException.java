package ru.ifmo.billingservice.v1.services.exceptions;

import ru.ifmo.billingservice.v1.services.exceptions.commons.ClientException;

public class ClientConflictException extends ClientException {

    public ClientConflictException(Throwable cause) {
        super("Concurrent operation failed.", cause);
    }

    public ClientConflictException(String message, Throwable cause) {
        super(message, cause);
    }
}
