package ru.ifmo.billingservice.v1.configurations;

import io.swagger.annotations.Api;
import lombok.Generated;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static ru.ifmo.billingservice.v1.Api.API_BASE;
import static ru.ifmo.billingservice.v1.Api.API_VERSION;
import static springfox.documentation.builders.PathSelectors.regex;
import static springfox.documentation.builders.RequestHandlerSelectors.withClassAnnotation;

@Configuration
@Generated
@EnableSwagger2
public class SwaggerConfig {

    @Value("${project.version}")
    @SuppressWarnings("unused")
    private String projectVersion;
    @Value("${project.name}")
    @SuppressWarnings("unused")
    private String projectName;
    @Value("${project.description}")
    @SuppressWarnings("unused")
    private String projectDescription;

    private ApiInfo swaggerApiInfo() {
        return new ApiInfoBuilder()
                .version(projectVersion)
                .title(projectName)
                .description(projectDescription)
                .build();
    }

    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .useDefaultResponseMessages(false)
                .groupName(API_VERSION)
                .apiInfo(swaggerApiInfo())
                .select()
                .apis(withClassAnnotation(Api.class))
                .paths(regex(API_BASE + "/.*"))
                .build();
    }
}
