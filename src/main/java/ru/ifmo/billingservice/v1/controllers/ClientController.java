package ru.ifmo.billingservice.v1.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.ifmo.billingservice.v1.models.Client;
import ru.ifmo.billingservice.v1.resources.ClientData;
import ru.ifmo.billingservice.v1.resources.ClientResource;
import ru.ifmo.billingservice.v1.resources.assemblers.ClientResourceAssembler;
import ru.ifmo.billingservice.v1.resources.mappers.ClientDataMapper;
import ru.ifmo.billingservice.v1.services.ClientService;
import ru.ifmo.billingservice.v1.services.exceptions.ClientConflictException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientDataException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientNotFoundException;
import ru.ifmo.billingservice.v1.validators.PhoneNumber;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static ru.ifmo.billingservice.v1.Api.API_BASE;

@RestController
@RequestMapping(ClientController.CONTROLLER_BASE)
@Validated
@Api(tags = {"Client"})
public class ClientController {

    static final String CONTROLLER_BASE = API_BASE + "/client";

    private final ClientService clientService;
    private final ClientDataMapper clientDataMapper;
    private final ClientResourceAssembler clientResourceAssembler;

    @Autowired
    public ClientController(ClientService clientService, ClientDataMapper clientDataMapper,
                            ClientResourceAssembler clientResourceAssembler) {
        this.clientService = clientService;
        this.clientDataMapper = clientDataMapper;
        this.clientResourceAssembler = clientResourceAssembler;
    }

    @ExceptionHandler(ClientDataException.class)
    @SuppressWarnings("unused")
    public void handleClientDataException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(ClientNotFoundException.class)
    @SuppressWarnings("unused")
    public void handleClientNotFoundException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler(ClientConflictException.class)
    @SuppressWarnings("unused")
    public void handleClientUpdateConflictException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.CONFLICT.value());
    }

    /**
     * Registers a new client.
     *
     * @param clientData client data
     * @return a new client that was registered
     * @throws URISyntaxException ...
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "register", notes = "Registers a new client.")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 409, message = "Conflict"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClientResource> register(@Valid @RequestBody ClientData clientData) throws URISyntaxException {
        Client entity = clientService.register(clientDataMapper.toEntity(clientData));
        ClientResource resource = clientResourceAssembler.toResource(entity);
        return ResponseEntity.created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    /**
     * Gets the list of registered clients.
     *
     * @return the list of registered clients
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "getAll", notes = "Gets the list of registered clients.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ClientResource>> getAll() {
        List<Client> entities = clientService.getAll();
        List<ClientResource> resources = clientResourceAssembler.toResources(entities);
        return ResponseEntity.ok(resources);
    }

    /**
     * Gets the specified client.
     *
     * @param phoneNumber client phone number
     * @return the requested client
     */
    @GetMapping("/{phoneNumber}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "get", notes = "Gets the specified client.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClientResource> get(@PathVariable @PhoneNumber String phoneNumber) {
        Client entity = clientService.get(phoneNumber);
        ClientResource resource = clientResourceAssembler.toResource(entity);
        return ResponseEntity.ok(resource);
    }

    /**
     * Updates the specified client data.
     *
     * @param phoneNumber client phone number
     * @param clientData  client data
     * @return void
     */
    @PatchMapping("/{phoneNumber}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "update", notes = "Updates the specified client data.")
    @ApiResponses({
            @ApiResponse(code = 204, message = "No Content"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 409, message = "Conflict"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Object> update(@PathVariable @PhoneNumber String phoneNumber,
                                         @Valid @RequestBody ClientData clientData) {
        clientService.update(phoneNumber, clientDataMapper.toEntity(clientData));
        return ResponseEntity.noContent().build();
    }
}
