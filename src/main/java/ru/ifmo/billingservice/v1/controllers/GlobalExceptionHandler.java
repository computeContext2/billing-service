package ru.ifmo.billingservice.v1.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;

@ControllerAdvice(basePackages = "ru.ifmo.billingservice.v1.controllers")
public class GlobalExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    @SuppressWarnings("unused")
    public void handleConstraintViolationException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }
}
