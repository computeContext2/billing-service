package ru.ifmo.billingservice.v1.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.ifmo.billingservice.v1.models.Asset;
import ru.ifmo.billingservice.v1.resources.AssetData;
import ru.ifmo.billingservice.v1.resources.AssetRepayData;
import ru.ifmo.billingservice.v1.resources.AssetResource;
import ru.ifmo.billingservice.v1.resources.assemblers.AssetResourceAssembler;
import ru.ifmo.billingservice.v1.resources.mappers.AssetDataMapper;
import ru.ifmo.billingservice.v1.resources.mappers.AssetRepayDataMapper;
import ru.ifmo.billingservice.v1.services.AssetService;
import ru.ifmo.billingservice.v1.services.exceptions.AssetConflictException;
import ru.ifmo.billingservice.v1.services.exceptions.AssetIllegalStateException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientNotFoundException;
import ru.ifmo.billingservice.v1.validators.PhoneNumber;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

import static ru.ifmo.billingservice.v1.Api.API_BASE;

@RestController
@RequestMapping(AssetController.CONTROLLER_BASE)
@Validated
@Api(tags = {"Asset"})
public class AssetController {

    static final String CONTROLLER_BASE = API_BASE + "/client/{phoneNumber}/asset";

    private final AssetService assetService;
    private final AssetDataMapper assetDataMapper;
    private final AssetRepayDataMapper assetRepayDataMapper;
    private final AssetResourceAssembler assetResourceAssembler;

    @Autowired
    public AssetController(AssetService assetService,
                           AssetDataMapper assetDataMapper,
                           AssetRepayDataMapper assetRepayDataMapper,
                           AssetResourceAssembler assetResourceAssembler) {
        this.assetService = assetService;
        this.assetDataMapper = assetDataMapper;
        this.assetRepayDataMapper = assetRepayDataMapper;
        this.assetResourceAssembler = assetResourceAssembler;
    }

    @ExceptionHandler(ClientNotFoundException.class)
    @SuppressWarnings("unused")
    public void handleClientNotFoundException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler(AssetIllegalStateException.class)
    @SuppressWarnings("unused")
    public void handleAssetIllegalStateException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.FORBIDDEN.value());
    }

    @ExceptionHandler(AssetConflictException.class)
    @SuppressWarnings("unused")
    public void handleAssetConflictException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.CONFLICT.value());
    }

    /**
     * Adds a new asset to the client.
     *
     * @param phoneNumber client phone number
     * @param assetData   asset data
     * @return a new asset that was added
     */
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "add", notes = "Adds a new asset to the client.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 409, message = "Conflict"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AssetResource> add(@PathVariable @PhoneNumber String phoneNumber,
                                             @Valid @RequestBody AssetData assetData) {
        Asset entity = assetService.add(phoneNumber, assetDataMapper.toEntity(assetData));
        AssetResource resource = assetResourceAssembler.toResource(entity);
        return ResponseEntity.ok(resource);
    }

    /**
     * Gets the list of available assets.
     *
     * @param phoneNumber client phone number
     * @return the list of available assets
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "getAvailable", notes = "Gets the list of available assets.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<AssetResource>> getAvailable(@PathVariable @PhoneNumber String phoneNumber) {
        List<Asset> entities = assetService.getAvailable(phoneNumber);
        List<AssetResource> resources = assetResourceAssembler.toResources(entities);
        return ResponseEntity.ok(resources);
    }

    /**
     * Repays the specified amount from available assets and returns remaining data.
     *
     * @param phoneNumber    client phone number
     * @param assetRepayData asset repay data
     * @return the list of available assets
     */
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "repay", notes = "Repays the specified amount from available assets and returns remaining data.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 409, message = "Conflict"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<AssetResource>> repay(@PathVariable @PhoneNumber String phoneNumber,
                                                     @Valid @RequestBody AssetRepayData assetRepayData) {
        List<Asset> entities = assetService.repay(phoneNumber,
                assetRepayDataMapper.toAssetType(assetRepayData.getAssetType()),
                assetRepayDataMapper.toEntity(assetRepayData));
        List<AssetResource> resources = assetResourceAssembler.toResources(entities);
        return ResponseEntity.ok(resources);
    }
}
