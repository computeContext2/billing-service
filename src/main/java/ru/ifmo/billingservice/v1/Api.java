package ru.ifmo.billingservice.v1;

public final class Api {

    public static final String API_VERSION = "v1";
    public static final String API_BASE = "/api/" + API_VERSION;

    private Api() {
    }
}
