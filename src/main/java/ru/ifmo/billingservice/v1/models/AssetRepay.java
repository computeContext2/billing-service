package ru.ifmo.billingservice.v1.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.ifmo.billingservice.v1.models.commons.Basic;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class AssetRepay extends Basic {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false, updatable = false)
    private Asset asset;

    @Column(nullable = false)
    private Double value;
}
