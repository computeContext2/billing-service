package ru.ifmo.billingservice.v1.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.ifmo.billingservice.v1.models.commons.Updatable;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(indexes = {
        @Index(columnList = "balance, validTo")
})
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class Asset extends Updatable {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false, updatable = false)
    private Client client;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AssetType assetType;

    @Column(nullable = false)
    private OffsetDateTime validFrom;

    @Column(nullable = false)
    private OffsetDateTime validTo;

    @Column(nullable = false)
    private Double initBalance;

    @Column(nullable = false)
    private Double balance;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "asset")
    private List<AssetRepay> assetRepays = new ArrayList<>();

    public enum AssetType {
        MINUTE,
        MESSAGE,
        MEGABYTE
    }
}
