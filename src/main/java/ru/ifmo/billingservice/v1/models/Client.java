package ru.ifmo.billingservice.v1.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.ifmo.billingservice.v1.models.commons.Updatable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class Client extends Updatable {

    @Column(length = 15, nullable = false, unique = true)
    private String phoneNumber;

    @Column(nullable = false)
    private Boolean isLocked;
}
