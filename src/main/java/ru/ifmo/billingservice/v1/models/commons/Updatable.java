package ru.ifmo.billingservice.v1.models.commons;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.time.OffsetDateTime;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public abstract class Updatable extends Basic {

    @UpdateTimestamp
    @Column(nullable = false)
    private OffsetDateTime lastModified;

    @Version
    @Column(nullable = false)
    private Long version;
}
