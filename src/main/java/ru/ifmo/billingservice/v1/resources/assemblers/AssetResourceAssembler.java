package ru.ifmo.billingservice.v1.resources.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import ru.ifmo.billingservice.v1.controllers.AssetController;
import ru.ifmo.billingservice.v1.models.Asset;
import ru.ifmo.billingservice.v1.resources.AssetResource;
import ru.ifmo.billingservice.v1.resources.AssetType;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class AssetResourceAssembler extends ResourceAssemblerSupport<Asset, AssetResource> {

    AssetResourceAssembler() {
        super(AssetController.class, AssetResource.class);
    }

    @Override
    public AssetResource toResource(Asset entity) {
        AssetResource resource = super.instantiateResource(entity);

        resource.setAssetType(AssetType.valueOf(entity.getAssetType().name()));
        resource.setValidFrom(entity.getValidFrom());
        resource.setValidTo(entity.getValidTo());
        resource.setInitBalance(entity.getInitBalance());
        resource.setBalance(entity.getBalance());

        return resource;
    }

    @Override
    public List<AssetResource> toResources(Iterable<? extends Asset> entities) {
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toResource)
                .collect(Collectors.toList());
    }
}
