package ru.ifmo.billingservice.v1.resources.mappers;

import org.springframework.stereotype.Component;
import ru.ifmo.billingservice.v1.models.Asset;
import ru.ifmo.billingservice.v1.models.AssetRepay;
import ru.ifmo.billingservice.v1.resources.AssetRepayData;
import ru.ifmo.billingservice.v1.resources.AssetType;

@Component
public class AssetRepayDataMapper {

    public Asset.AssetType toAssetType(AssetType assetType) {
        return Asset.AssetType.valueOf(assetType.name());
    }

    public AssetRepay toEntity(AssetRepayData assetRepayData) {
        AssetRepay entity = new AssetRepay();
        entity.setValue(assetRepayData.getValue());
        return entity;
    }
}
