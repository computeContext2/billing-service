package ru.ifmo.billingservice.v1.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.ifmo.billingservice.v1.validators.PhoneNumber;

@Data
@EqualsAndHashCode
@ToString
@ApiModel(description = "Client data")
public class ClientData {

    @PhoneNumber
    @ApiModelProperty("Unique client phone number in E.164 format")
    private String phoneNumber;

    @ApiModelProperty("The new status of the client lockout")
    private Boolean isLocked;
}
