package ru.ifmo.billingservice.v1.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.OffsetDateTime;

@Data
@EqualsAndHashCode
@ToString
@ApiModel(description = "Asset registration data")
public class AssetData {

    @NotNull
    @ApiModelProperty("Asset type")
    private AssetType assetType;

    @NotNull
    @FutureOrPresent
    @ApiModelProperty("Start date (inclusive)")
    private OffsetDateTime validFrom;

    @NotNull
    @Future
    @ApiModelProperty("End date (exclusive)")
    private OffsetDateTime validTo;

    @NotNull
    @Positive
    @ApiModelProperty("Asset balance")
    private Double balance;
}
