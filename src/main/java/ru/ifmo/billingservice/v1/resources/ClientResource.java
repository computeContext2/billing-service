package ru.ifmo.billingservice.v1.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.hateoas.ResourceSupport;

import java.time.OffsetDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@ApiModel(description = "Client resource")
public class ClientResource extends ResourceSupport {

    @ApiModelProperty("Unique client phone number in E.164 format")
    private String phoneNumber;

    @ApiModelProperty("The current status of the client lockout")
    private Boolean isLocked;

    @ApiModelProperty("The date on which the client was registered")
    private OffsetDateTime dateRegistered;
}
