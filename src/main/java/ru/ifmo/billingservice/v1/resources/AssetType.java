package ru.ifmo.billingservice.v1.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Asset type")
public enum AssetType {
    @ApiModelProperty("One minute")
    MINUTE,
    @ApiModelProperty("Short message")
    MESSAGE,
    @ApiModelProperty("One megabyte of traffic")
    MEGABYTE
}
