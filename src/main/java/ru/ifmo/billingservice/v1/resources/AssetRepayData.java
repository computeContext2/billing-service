package ru.ifmo.billingservice.v1.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@EqualsAndHashCode
@ToString
@ApiModel(description = "Asset repayment data")
public class AssetRepayData {

    @NotNull
    @ApiModelProperty("Asset type")
    private AssetType assetType;

    @NotNull
    @Positive
    @ApiModelProperty("Repayment value")
    private Double value;
}
