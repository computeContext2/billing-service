package ru.ifmo.billingservice.v1.resources.mappers;

import org.springframework.stereotype.Component;
import ru.ifmo.billingservice.v1.models.Client;
import ru.ifmo.billingservice.v1.resources.ClientData;

@Component
public class ClientDataMapper {

    public Client toEntity(ClientData clientData) {
        Client entity = new Client();

        entity.setPhoneNumber(clientData.getPhoneNumber());
        entity.setIsLocked(clientData.getIsLocked());

        return entity;
    }
}
