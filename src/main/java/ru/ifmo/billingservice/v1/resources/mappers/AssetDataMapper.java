package ru.ifmo.billingservice.v1.resources.mappers;

import org.springframework.stereotype.Component;
import ru.ifmo.billingservice.v1.models.Asset;
import ru.ifmo.billingservice.v1.resources.AssetData;

@Component
public class AssetDataMapper {

    public Asset toEntity(AssetData assetData) {
        Asset entity = new Asset();

        entity.setAssetType(Asset.AssetType.valueOf(assetData.getAssetType().name()));
        entity.setValidFrom(assetData.getValidFrom());
        entity.setValidTo(assetData.getValidTo());
        entity.setInitBalance(assetData.getBalance());

        return entity;
    }
}
