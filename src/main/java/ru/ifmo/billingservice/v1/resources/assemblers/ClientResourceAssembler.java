package ru.ifmo.billingservice.v1.resources.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import ru.ifmo.billingservice.v1.controllers.ClientController;
import ru.ifmo.billingservice.v1.models.Client;
import ru.ifmo.billingservice.v1.resources.ClientResource;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ClientResourceAssembler extends ResourceAssemblerSupport<Client, ClientResource> {

    ClientResourceAssembler() {
        super(ClientController.class, ClientResource.class);
    }

    @Override
    public ClientResource toResource(Client entity) {
        ClientResource resource = super.createResourceWithId(entity.getPhoneNumber(), entity);
        resource.add(linkTo(methodOn(ClientController.class).getAll()).withRel("all"));

        resource.setPhoneNumber(entity.getPhoneNumber());
        resource.setIsLocked(entity.getIsLocked());
        resource.setDateRegistered(entity.getCreated());

        return resource;
    }

    @Override
    public List<ClientResource> toResources(Iterable<? extends Client> entities) {
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toResource)
                .collect(Collectors.toList());
    }
}
