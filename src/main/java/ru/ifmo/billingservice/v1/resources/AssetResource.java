package ru.ifmo.billingservice.v1.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.hateoas.ResourceSupport;

import java.time.OffsetDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@ApiModel(description = "Asset resource")
public class AssetResource extends ResourceSupport {

    @ApiModelProperty("Asset type")
    private AssetType assetType;

    @ApiModelProperty("Start date (inclusive)")
    private OffsetDateTime validFrom;

    @ApiModelProperty("End date (exclusive)")
    private OffsetDateTime validTo;

    @ApiModelProperty("Asset start balance")
    private Double initBalance;

    @ApiModelProperty("Asset current balance")
    private Double balance;
}
