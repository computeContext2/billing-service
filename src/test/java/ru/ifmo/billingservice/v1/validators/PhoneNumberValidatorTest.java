package ru.ifmo.billingservice.v1.validators;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.google.i18n.phonenumbers.NumberParseException.ErrorType;
import static com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class PhoneNumberValidatorTest {

    @Mock
    private PhoneNumberUtil phoneNumberUtil;

    @InjectMocks
    private PhoneNumberValidator phoneNumberValidator;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void phoneNumberIsNotPresent() {
        // act
        boolean isValid = phoneNumberValidator.isValid(null, null);

        // assert
        assertTrue(isValid);
    }

    @Test
    public void phoneNumberParseError() throws NumberParseException {
        // arrange
        String phoneNumber = "123";
        when(phoneNumberUtil.parse(eq(phoneNumber), anyString())).thenThrow(
                new NumberParseException(ErrorType.NOT_A_NUMBER, null));

        // act
        boolean isValid = phoneNumberValidator.isValid(phoneNumber, null);

        // assert
        assertFalse(isValid);
    }

    @Test
    public void phoneNumberIsNotValid() {
        // arrange
        String phoneNumber = "123";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(false);

        // act
        boolean isValid = phoneNumberValidator.isValid(phoneNumber, null);

        // assert
        assertFalse(isValid);
    }

    @Test
    public void phoneNumberIsNotE164() {
        // arrange
        String phoneNumber = "123";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), eq(PhoneNumberFormat.E164))).thenReturn("");

        // act
        boolean isValid = phoneNumberValidator.isValid(phoneNumber, null);

        // assert
        assertFalse(isValid);
    }

    @Test
    public void phoneNumberIsValidE164() {
        // arrange
        String phoneNumber = "+79001234567";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), eq(PhoneNumberFormat.E164))).thenReturn(phoneNumber);

        // act
        boolean isValid = phoneNumberValidator.isValid(phoneNumber, null);

        // assert
        assertTrue(isValid);
    }
}