package ru.ifmo.billingservice.v1.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.OptimisticLockingFailureException;
import ru.ifmo.billingservice.v1.models.Client;
import ru.ifmo.billingservice.v1.repositories.ClientRepository;
import ru.ifmo.billingservice.v1.services.exceptions.ClientConflictException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientDataException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientService clientService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void registerIfAllOk() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(client.getPhoneNumber()).thenReturn(phoneNumber);
        when(client.getIsLocked()).thenReturn(true);
        when(clientRepository.save(client)).thenReturn(client);

        // act
        Client entity = clientService.register(client);

        // assert
        assertSame(client, entity);
        assertEquals(phoneNumber, entity.getPhoneNumber());
        assertTrue(entity.getIsLocked());
    }

    @Test
    public void registerIfClientIsLockedIsNotPresent() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(client.getPhoneNumber()).thenReturn(phoneNumber);
        when(client.getIsLocked()).thenReturn(null);
        when(clientRepository.save(client)).thenReturn(client);

        // act
        Client entity = clientService.register(client);

        // assert
        assertSame(client, entity);
        assertEquals(phoneNumber, entity.getPhoneNumber());
        verify(client, times(1)).setIsLocked(false);
    }

    @Test(expected = NullPointerException.class)
    public void registerIfClientIsNull() {
        // act
        // noinspection ConstantConditions
        clientService.register(null);
    }

    @Test(expected = ClientDataException.class)
    public void registerIfClientPhoneNumberIsNotPresent() {
        // arrange
        Client client = mock(Client.class);
        when(client.getPhoneNumber()).thenReturn(null);

        // act
        clientService.register(client);
    }

    @Test(expected = ClientConflictException.class)
    public void registerIfDataIntegrityViolationThrows() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(client.getPhoneNumber()).thenReturn(phoneNumber);
        // noinspection ConstantConditions
        when(clientRepository.save(client)).thenThrow(new DataIntegrityViolationException(null));

        // act
        clientService.register(client);
    }

    @Test(expected = ClientConflictException.class)
    public void registerIfOptimisticLockingFailureThrows() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(client.getPhoneNumber()).thenReturn(phoneNumber);
        // noinspection ConstantConditions
        when(clientRepository.save(client)).thenThrow(new OptimisticLockingFailureException(null));

        // act
        clientService.register(client);
    }

    @Test
    public void getAll() {
        // arrange
        Client client = mock(Client.class);
        when(clientRepository.findAll()).thenReturn(Stream.of(client).collect(Collectors.toList()));

        // act
        List<Client> entities = clientService.getAll();

        // assert
        assertNotNull(entities);
        assertThat(entities, hasSize(1));
    }

    @Test
    public void getIfAllOk() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(clientRepository.findByPhoneNumber(phoneNumber)).thenReturn(Optional.of(client));

        // act
        Client entity = clientService.get(phoneNumber);

        // assert
        assertSame(client, entity);
    }

    @Test(expected = NullPointerException.class)
    public void getIfClientPhoneNumberIsNull() {
        // act
        clientService.get(null);
    }

    @Test(expected = ClientNotFoundException.class)
    public void getIfClientDoesNotExist() {
        // arrange
        String phoneNumber = "+79001234567";
        when(clientRepository.findByPhoneNumber(phoneNumber)).thenReturn(Optional.empty());

        // act
        clientService.get(phoneNumber);
    }

    @Test
    public void updateIfAllOk() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(client.getPhoneNumber()).thenReturn(phoneNumber);
        when(client.getIsLocked()).thenReturn(true);
        when(clientRepository.findByPhoneNumber(phoneNumber)).thenReturn(Optional.of(client));
        when(clientRepository.save(client)).thenReturn(client);

        // act
        clientService.update(phoneNumber, client);

        // assert
        verify(client, times(1)).setIsLocked(true);
        verify(clientRepository, times(1)).save(client);
    }

    @Test
    public void updateIfClientIsLockedIsNotPresent() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(client.getPhoneNumber()).thenReturn(phoneNumber);
        when(client.getIsLocked()).thenReturn(null);
        when(clientRepository.findByPhoneNumber(phoneNumber)).thenReturn(Optional.of(client));
        when(clientRepository.save(client)).thenReturn(client);

        // act
        clientService.update(phoneNumber, client);

        // assert
        verify(client, never()).setIsLocked(any());
        verify(clientRepository, times(1)).save(client);
    }

    @Test(expected = NullPointerException.class)
    public void updateIfClientPhoneNumberIsNull() {
        // arrange
        Client client = mock(Client.class);

        // act
        clientService.update(null, client);
    }

    @Test(expected = NullPointerException.class)
    public void updateIfClientIsNull() {
        // arrange
        String phoneNumber = "+79001234567";

        // act
        // noinspection ConstantConditions
        clientService.update(phoneNumber, null);
    }

    @Test(expected = ClientDataException.class)
    public void updateIfClientPhoneNumberNotMatch() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(client.getPhoneNumber()).thenReturn("");

        // act
        clientService.update(phoneNumber, client);
    }

    @Test(expected = ClientNotFoundException.class)
    public void updateIfClientDoesNotExist() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(clientRepository.findByPhoneNumber(phoneNumber)).thenReturn(Optional.empty());

        // act
        clientService.update(phoneNumber, client);
    }

    @Test(expected = ClientConflictException.class)
    public void updateIfDataIntegrityViolationThrows() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(clientRepository.findByPhoneNumber(phoneNumber)).thenReturn(Optional.of(client));
        // noinspection ConstantConditions
        when(clientRepository.save(client)).thenThrow(new DataIntegrityViolationException(null));

        // act
        clientService.update(phoneNumber, client);
    }

    @Test(expected = ClientConflictException.class)
    public void updateIfOptimisticLockingFailureThrows() {
        // arrange
        String phoneNumber = "+79001234567";
        Client client = mock(Client.class);
        when(clientRepository.findByPhoneNumber(phoneNumber)).thenReturn(Optional.of(client));
        // noinspection ConstantConditions
        when(clientRepository.save(client)).thenThrow(new OptimisticLockingFailureException(null));

        // act
        clientService.update(phoneNumber, client);
    }
}