package ru.ifmo.billingservice.v1.resources.assemblers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.hateoas.Link;
import ru.ifmo.billingservice.v1.models.Client;
import ru.ifmo.billingservice.v1.resources.ClientResource;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class ClientResourceAssemblerTest {

    @Spy
    @InjectMocks
    private ClientResourceAssembler clientResourceAssembler;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void toResource() {
        // arrange
        String phoneNumber = "+79001234567";
        Client entity = mock(Client.class);
        when(entity.getPhoneNumber()).thenReturn(phoneNumber);

        // act
        ClientResource resource = clientResourceAssembler.toResource(entity);

        // assert
        assertNotNull(resource);
        assertTrue(resource.hasLink(Link.REL_SELF));
        assertEquals(phoneNumber, resource.getPhoneNumber());
    }

    @Test
    public void toResources() {
        // arrange
        String phoneNumber = "+79001234567";
        Client entity = mock(Client.class);
        when(entity.getPhoneNumber()).thenReturn(phoneNumber);

        List<Client> entities = Stream.of(entity).collect(Collectors.toList());

        // act
        List<ClientResource> resources = clientResourceAssembler.toResources(entities);

        // assert
        assertNotNull(resources);
        assertThat(resources, hasSize(1));
        verify(clientResourceAssembler, times(1)).toResource(entity);
    }
}