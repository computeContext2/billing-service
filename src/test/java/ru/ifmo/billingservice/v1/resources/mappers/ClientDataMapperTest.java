package ru.ifmo.billingservice.v1.resources.mappers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import ru.ifmo.billingservice.v1.models.Client;
import ru.ifmo.billingservice.v1.resources.ClientData;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ClientDataMapperTest {

    @InjectMocks
    private ClientDataMapper clientDataMapper;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void toEntity() {
        // arrange
        String phoneNumber = "+79001234567";
        ClientData clientData = mock(ClientData.class);
        when(clientData.getPhoneNumber()).thenReturn(phoneNumber);
        when(clientData.getIsLocked()).thenReturn(true);

        // act
        Client entity = clientDataMapper.toEntity(clientData);

        // assert
        assertNotNull(entity);
        assertEquals(phoneNumber, entity.getPhoneNumber());
        assertTrue(entity.getIsLocked());
    }
}