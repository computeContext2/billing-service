package ru.ifmo.billingservice.v1.controllers;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.ifmo.billingservice.v1.resources.ClientResource;
import ru.ifmo.billingservice.v1.resources.assemblers.ClientResourceAssembler;
import ru.ifmo.billingservice.v1.resources.mappers.ClientDataMapper;
import ru.ifmo.billingservice.v1.services.ClientService;
import ru.ifmo.billingservice.v1.services.exceptions.ClientConflictException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientDataException;
import ru.ifmo.billingservice.v1.services.exceptions.ClientNotFoundException;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientController.class)
public class ClientControllerTest {

    @MockBean
    @SuppressWarnings("unused")
    private PhoneNumberUtil phoneNumberUtil;
    @MockBean
    @SuppressWarnings("unused")
    private ClientService clientService;
    @MockBean
    @SuppressWarnings("unused")
    private ClientDataMapper clientDataMapper;
    @MockBean
    @SuppressWarnings("unused")
    private ClientResourceAssembler clientResourceAssembler;

    @Autowired
    @SuppressWarnings("unused")
    private MockMvc mockMvc;

    @Test
    public void registerIsCreated() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";
        String href = "http://localhost" + ClientController.CONTROLLER_BASE + "/" + phoneNumber;
        ClientResource resource = new ClientResource();
        resource.add(new Link(href, Link.REL_SELF));
        when(clientResourceAssembler.toResource(any())).thenReturn(resource);

        // act, assert
        mockMvc.perform(post(ClientController.CONTROLLER_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, href))
                .andExpect(jsonPath("$._links.self.href", is(href)));
    }

    @Test
    public void registerIsBadRequest() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";
        when(clientService.register(any())).thenThrow(new ClientDataException(null));

        // act, assert
        mockMvc.perform(post(ClientController.CONTROLLER_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void registerIsBadRequestOnJsr303Validation() throws Exception {
        // arrange
        String phoneNumber = "123";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(false);
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";

        // act, assert
        mockMvc.perform(post(ClientController.CONTROLLER_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void registerIsConflict() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";
        when(clientService.register(any())).thenThrow(new ClientConflictException(null, null));

        // act, assert
        mockMvc.perform(post(ClientController.CONTROLLER_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void getAllIsOk() throws Exception {
        mockMvc.perform(get(ClientController.CONTROLLER_BASE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getIsOk() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);
        ClientResource resource = new ClientResource();
        resource.setPhoneNumber(phoneNumber);
        when(clientResourceAssembler.toResource(any())).thenReturn(resource);

        // act, assert
        mockMvc.perform(get(ClientController.CONTROLLER_BASE + "/{phoneNumber}", phoneNumber))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.phoneNumber", is(phoneNumber)));
    }

    @Test
    public void getIsBadRequest() throws Exception {
        // arrange
        String phoneNumber = "123";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(false);

        // act, assert
        mockMvc.perform(get(ClientController.CONTROLLER_BASE + "/{phoneNumber}", phoneNumber))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getIsNotFound() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);
        when(clientService.get(phoneNumber)).thenThrow(new ClientNotFoundException());

        // act, assert
        mockMvc.perform(get(ClientController.CONTROLLER_BASE + "/{phoneNumber}", phoneNumber))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateIsNoContent() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);

        // act, assert
        mockMvc.perform(patch(ClientController.CONTROLLER_BASE + "/{phoneNumber}", phoneNumber)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateIsBadRequest() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";
        doThrow(new ClientDataException(null)).when(clientService).update(eq(phoneNumber), any());

        // act, assert
        mockMvc.perform(patch(ClientController.CONTROLLER_BASE + "/{phoneNumber}", phoneNumber)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateIsBadRequestOnJsr303Validation() throws Exception {
        // arrange
        String phoneNumber = "123";
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(false);

        // act, assert
        mockMvc.perform(patch(ClientController.CONTROLLER_BASE + "/{phoneNumber}", phoneNumber)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateIsNotFound() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);
        doThrow(new ClientNotFoundException()).when(clientService).update(eq(phoneNumber), any());

        // act, assert
        mockMvc.perform(patch(ClientController.CONTROLLER_BASE + "/{phoneNumber}", phoneNumber)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateIsConflict() throws Exception {
        // arrange
        String phoneNumber = "+79001234567";
        String content = "{\"phoneNumber\":\"" + phoneNumber + "\"}";
        when(phoneNumberUtil.isValidNumber(any())).thenReturn(true);
        when(phoneNumberUtil.format(any(), any())).thenReturn(phoneNumber);
        doThrow(new ClientConflictException(null)).when(clientService).update(eq(phoneNumber), any());

        // act, assert
        mockMvc.perform(patch(ClientController.CONTROLLER_BASE + "/{phoneNumber}", phoneNumber)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isConflict());
    }
}