package ru.ifmo.billingservice.v1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.ifmo.billingservice.v1.controllers.AssetController;
import ru.ifmo.billingservice.v1.controllers.ClientController;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmokeTest {

    @Autowired
    @SuppressWarnings("unused")
    private ClientController clientController;

    @Autowired
    @SuppressWarnings("unused")
    private AssetController assetController;

    @Test
    public void contextLoads() {
        // assert
        assertThat(clientController).isNotNull();
        assertThat(assetController).isNotNull();
    }
}
