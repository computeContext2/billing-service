FROM amazoncorretto:11
COPY build/libs/*.jar /opt/ru.ifmo/
WORKDIR /opt/ru.ifmo
EXPOSE 8080/tcp
ENTRYPOINT java -jar "$(ls billing-service-*.jar | sort -V | tail -n1)"
