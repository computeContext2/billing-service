# Billing Service
This is a simple billing service written in Java / Spring Boot.

The service provides the following features:
  - Client registration and lockout
  - Assets accrual and write-off (e.g. minutes, messages, traffic)
  - Balance reporting

### Build
Project requires [JDK 11](http://www.oracle.com/technetwork/java/javase/downloads/index.html) and Gradle to build.
Gradle is already bundled. Install the JDK and execute the following commands:
```sh
$ cd ./billing-service
$ ./gradlew clean build
```

### Run
To start the application, run the following commands:
```sh
$ cd ./build/libs
$ java -jar billing-service-${project.version}.jar
```
Then open your favorite browser and follow the link for documentation http://localhost:8080/swagger-ui.html

License
----
The Unlicense
